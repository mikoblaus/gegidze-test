import React, {useEffect, useState} from 'react';

type Props = {
    todos?: TodoItem[];
};

type TodoItem = {
    id: any;
    title: string;
    isDone?: boolean;
};


function ExpensiveTree() {
    let now = performance.now();

    while (performance.now() - now < 1000) {
        // Artificial delay -- do nothing for 1000ms
    } 
    return null;
}

const App: React.FC<Props> = (props) => {
    const [todos, setTodos] = useState<TodoItem[]>([]);
    const [filter, setFilter] = useState<'all' | 'undone'>('all');
    const [doneCount, setDoneCount] = useState(0);

    useEffect(() => {
        if (todos.length) {
            setFilter("all");
        }
    }, [todos]);

    const addTodo = () => {
        let title = prompt('What to do?');
        if (title) {
            let item = {
                id: Date.now(),
                title,
                isDone: false
            };
            setTodos([...todos, item]);
        }
    };
    const markAsDone = (todo: TodoItem) => {
        const todoItem = todos.find((item) => item.id === todo.id);
        if (todoItem) {
            todoItem.isDone = !todoItem.isDone;
        }
        const done = todos.filter(({isDone}) => isDone === true);
        setDoneCount(done.length);
    };

    const deleteTodo = (todo: TodoItem) => {
        const todoItems = todos.filter((item) => item.id !== todo.id);
        const done = todoItems.filter(({isDone}) => isDone === true);

        setTodos(todoItems);
        setDoneCount(done.length);
    };
    const onFilterButtonClick = () => {
        setFilter(filter === "all" ? "undone" : "all");
    };


    return (
        <>
            <p>{`${doneCount} / ${todos.length}`}</p>
            <ul>
                {todos
                    .filter((todo) => (filter === "all" ? true : !todo.isDone))
                    .map((todo, key) => (
                        <div key={`${todo.id}`}>
                            <p>{`${todo.isDone ? '✅ ' : ''}${todo.title}`}</p>
                            <button
                                onClick={() => {
                                    markAsDone(todo)
                                }}>
                                {
                                    todo.isDone ? 'UnDone' : 'Done'
                                }

                            </button>
                            <button onClick={() => deleteTodo(todo)}>{'Delete'}</button>
                        </div>
                    ))}

            </ul>
            <button onClick={() => addTodo()}>{'Add'}</button>
            <button onClick={() => onFilterButtonClick()}>
                {`Show ${filter === 'all' ? 'undone' : 'all'} todos`}
            </button>
            <ExpensiveTree />
        </>
    )
}


export default App;
